# QDeeplandia - QGIS Plugin

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)


[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)


QGIS plugin dedicated to 2D semantic segmentation for automated image analysis

For more details about 2D semantic segmentation in Python, have a look at [Deeposlandia](https://github.com/oslandia/deeposlandia), and play with our [demo app](http://data.oslandia.io/deeposlandia).

## Generated options

### Plugin

"plugin_name": QDeeplandia
"plugin_name_slug": qdeeplandia
"plugin_name_class": Qdeeplandia

"plugin_category": Database
"plugin_description_short": This plugin is a revolution!
"plugin_description_long": Extends QGIS with revolutionary features that every single GIS end-users was expected (or not)!
"plugin_description_short": This plugin is a revolution!
"plugin_icon": detection_mask.png

"author_name": Firstname LASTNAME
"author_email": qgis@oslandia.com

"qgis_version_min": 3.16
"qgis_version_max": 3.99

### Tooling

This project is configured with the following tools:

- [Black](https://black.readthedocs.io/en/stable/) to format the code without any existential question
- [iSort](https://pycqa.github.io/isort/) to sort the Python imports

Code rules are enforced with [pre-commit](https://pre-commit.com/) hooks.  
Static code analisis is based on: Flake8

See also: [contribution guidelines](CONTRIBUTING.md).

## CI/CD

Plugin is linted, tested and packaged with GitHub.


### Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages.

- homepage: <None>
- repository: <https://gitlab.com/Oslandia/qdeeplandia/>
- tracker: <None>

----

## License

Distributed under the terms of the [`GPLv2+` license](LICENSE).
