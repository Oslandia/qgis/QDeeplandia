# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->

	## Unreleased

	## 0.3.1 - 2021-12-23

	### Added

	- Add the deeposlandia inference output layer as new (optional)
	layers in the QGIS project. (#8)
	- Basic CI scheme. (#3)

	## 0.3.0 - 2021-12-23

	### Changed

	- Pass an image basename to the `deepo datagen` command, instead of a testing image quantity.
	- Change from `deepo postprocess` to `deepo geoinfer`.
	- Handle geojson files with valid CRS (Deeposlandia>v0.7.1), for predicted geometries.

	### Fixed

	- datagen_task and inference_task are maintained through a
	dedicated class attribute to avoid 'core dumped' crash. (#1)

	## 0.2.0 - 2021-12-21

	### Changed

	- Rebuilt from the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/).

	## 0.1.0 - 2020-06-19

	- First release
