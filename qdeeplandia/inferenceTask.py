import os

import processing
from qgis.core import QgsMessageLog, QgsTask
from qgis.PyQt.QtCore import pyqtSignal

from qdeeplandia.feedback import Feedback


class InferenceTask(QgsTask):
    """InferenceTask is a QgsTask subclass"""

    terminated = pyqtSignal(str)

    def __init__(self, description, iface, layer, imsize, batch_size):
        super().__init__(description, QgsTask.CanCancel)
        self.id = "infer-" + layer.id()
        self.feedback = Feedback(iface)
        tmp_name = processing.getTempFilename() + ".tif"
        self.param = {
            "INPUT": layer.id(),
            "OUTPUT": os.path.join(tmp_name),
            "IMAGE_SIZE": imsize,
            "BATCH_SIZE": batch_size,
        }

    def run(self):
        QgsMessageLog.logMessage(
            'Task "{name}" is starting'.format(name=self.description()), "QDeeplandia"
        )
        out = processing.run(
            "qdeeplandia:InferenceQDeepLandia", self.param, feedback=self.feedback
        )
        if os.path.exists(out["OUTPUT"]):
            self.terminated.emit(out["OUTPUT"])
        return True

    def cancel(self):
        QgsMessageLog.logMessage(
            'Task "{name}" was canceled'.format(name=self.description()), "QDeeplandia"
        )
        self.terminated.emit(None)
        super().cancel()
