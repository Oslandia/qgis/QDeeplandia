"""QgsTask dedicated to the generation of a valid deeposlandia dataset
"""

import os

import processing
from qgis.core import QgsMessageLog, QgsTask
from qgis.PyQt.QtCore import pyqtSignal

from qdeeplandia.feedback import Feedback


class DatagenTask(QgsTask):

    terminated = pyqtSignal(str)

    def __init__(self, description, iface, layer, imsize):
        super().__init__(description, QgsTask.CanCancel)
        self.id = "datagen-" + layer.id()
        self.feedback = Feedback(iface)
        self.param = {
            "INPUT": layer.id(),
            "IMAGE_SIZE": imsize,
            "OUTPUT": os.path.dirname(processing.getTempFilename()),
        }

    def run(self):
        out = {}
        out[
            "OUTPUT"
        ] = "/home/rdelhome/Documents/projets/plugin_qdeeplandia/data_qdeeplandia/tanzania"
        out = processing.run(
            "qdeeplandia:DatagenQDeepLandia", self.param, feedback=self.feedback
        )
        if os.path.exists(out["OUTPUT"]):
            self.terminated.emit(out["OUTPUT"])
        return True

    def cancel(self):
        QgsMessageLog.logMessage(
            'Task "{name}" was canceled'.format(name=self.description()), "QDeeplandia"
        )
        self.terminated.emit(None)
        super().cancel()
