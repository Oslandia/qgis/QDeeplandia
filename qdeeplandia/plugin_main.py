#! python3  # noqa: E265

"""
    Main plugin module.
"""

import os
from functools import partial

from qgis.core import QgsApplication, QgsMessageLog, QgsRasterDataProvider
from qgis.gui import QgisInterface
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QLabel, QToolBar, QWidget
from qgis.utils import showPluginHelp

# project
from qdeeplandia.__about__ import __title__
from qdeeplandia.datagenTask import DatagenTask
from qdeeplandia.gui.datagenParamDialog import DatagenParamDialog
from qdeeplandia.gui.dlg_settings import PlgOptionsFactory
from qdeeplandia.gui.inferenceParamDialog import InferenceParamDialog
from qdeeplandia.inferenceTask import InferenceTask
from qdeeplandia.processing import QdeeplandiaProvider
from qdeeplandia.toolbelt import PlgLogger, PlgTranslator

os.environ["DEEPOSL_CONFIG"] = os.path.join(os.path.dirname(__file__), "config.ini")


# ############################################################################
# ########## Classes ###############
# ##################################


class QdeeplandiaPlugin(QWidget):
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        super(QdeeplandiaPlugin, self).__init__()
        self.iface = iface
        self.mapCanvas = self.iface.mapCanvas()

        self.updateLayer()
        self.log = PlgLogger().log
        self.provider = None

        # translation
        plg_translation_mngr = PlgTranslator()
        translator = plg_translation_mngr.get_translator()
        if translator:
            QCoreApplication.installTranslator(translator)
        self.tr = plg_translation_mngr.tr
        #
        self.mapCanvas.currentLayerChanged.connect(self.updateLayer)
        #
        self.dataset_path = None
        self.image_size = None
        #
        self._tasks = dict()

    def initGui(self):
        """Set up plugin UI elements."""

        # settings page within the QGIS preferences menu
        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

        # -- Actions
        # Data preparation process
        dataprep_msg = self.tr("Prepare a dataset")
        load_icon = QIcon(
            os.path.join(os.path.dirname(__file__), "resources/images/load.svg")
        )
        self.dataset_preparation = QAction(
            load_icon, dataprep_msg, self.iface.mainWindow()
        )
        self.dataset_preparation.triggered.connect(lambda: self.prepare_dataset())
        # Run-an-inference process
        run_inference_msg = self.tr("Run an inference")
        run_icon = QIcon(
            os.path.join(os.path.dirname(__file__), "resources/images/run.svg")
        )
        self.inference = QAction(run_icon, run_inference_msg, self.iface.mainWindow())
        self.inference.triggered.connect(lambda: self.infer())
        # Help
        self.action_help = QAction(
            QIcon(":/images/themes/default/mActionHelpContents.svg"),
            self.tr("Help", context="QdeeplandiaPlugin"),
            self.iface.mainWindow(),
        )
        self.action_help.triggered.connect(
            lambda: showPluginHelp(filename="resources/help/index")
        )
        # Settings
        self.action_settings = QAction(
            QgsApplication.getThemeIcon("console/iconSettingsConsole.svg"),
            self.tr("Settings"),
            self.iface.mainWindow(),
        )
        self.action_settings.triggered.connect(
            lambda: self.iface.showOptionsDialog(
                currentPage="mOptionsPage{}".format(__title__)
            )
        )

        # -- Processing
        self.initProcessing()

        # -- Menu
        self.iface.addPluginToMenu(__title__, self.dataset_preparation)
        self.iface.addPluginToMenu(__title__, self.inference)
        self.iface.addPluginToMenu(__title__, self.action_settings)
        self.iface.addPluginToMenu(__title__, self.action_help)

        # -- Toolbar
        self.toolbar = QToolBar(self.tr("QDeepLandia_toolbar"))
        self.toolbar.setObjectName("QDeepLandia_toolbar")
        self.toolbar.addWidget(QLabel(self.tr("QDeeplandia")))
        self.toolbar.addAction(self.dataset_preparation)
        self.toolbar.addAction(self.inference)
        self.toolbar.setVisible(True)
        self.iface.addToolBar(self.toolbar)

    def initProcessing(self):
        self.provider = QdeeplandiaProvider()
        QgsApplication.processingRegistry().addProvider(self.provider)

    def unload(self):
        """Cleans up when plugin is disabled/uninstalled."""
        # -- Clean up menu
        self.iface.removePluginMenu(__title__, self.dataset_preparation)
        self.iface.removePluginMenu(__title__, self.inference)
        self.iface.removePluginMenu(__title__, self.action_help)
        self.iface.removePluginMenu(__title__, self.action_settings)

        # -- Clean up preferences panel in QGIS settings
        self.iface.unregisterOptionsWidgetFactory(self.options_factory)

        # -- Unregister processing
        QgsApplication.processingRegistry().removeProvider(self.provider)

        # remove actions
        del self.action_settings
        del self.action_help

        self.toolbar.setParent(None)
        self.dataset_preparation.setParent(None)
        self.inference.setParent(None)

    def prepare_dataset(self):
        """Prepare a Deeposlandia dataset, as a prerequisite before infering any result."""

        def highlight_datapath(path):
            if os.path.exists(path):
                print("Dataset path:", path)
            else:
                print("Path", path, "does not exist.")

        if not self.layer:
            QgsMessageLog.logMessage("Unvalid layer!")
            return
        datagenParamDlg = DatagenParamDialog(self)
        if datagenParamDlg.exec():
            self.image_size = datagenParamDlg.imsize()
        else:
            return
        print("run datagen task with image size:", self.image_size)
        datagen_task = DatagenTask("Datagen", self.iface, self.layer, self.image_size)
        self._tasks[datagen_task.id] = datagen_task
        self._tasks[datagen_task.id].taskCompleted.connect(
            partial(self._tasks.pop, datagen_task.id)
        )
        self._tasks[datagen_task.id].terminated.connect(highlight_datapath)
        QgsApplication.taskManager().addTask(datagen_task)
        print("End of the datagen process!")

    def infer(self):
        """Launch inference on the current layer"""

        def addOutput(layer):
            if layer:
                self.iface.addVectorLayer(layer, "predictions", "ogr")

        inferenceParamDlg = InferenceParamDialog(self)
        if inferenceParamDlg.exec():
            self.image_size = inferenceParamDlg.imsize()
            self.batch_size = inferenceParamDlg.batchsize()
        else:
            return

        print(
            "beginning of the inference:", self.layer, self.image_size, self.batch_size
        )
        if not self.layer:
            QgsMessageLog.logMessage("Unvalid layer!")
            return
        inference_task = InferenceTask(
            "Inference", self.iface, self.layer, self.image_size, self.batch_size
        )
        self._tasks[inference_task.id] = inference_task
        self._tasks[inference_task.id].taskCompleted.connect(
            partial(self._tasks.pop, inference_task.id)
        )
        self._tasks[inference_task.id].terminated.connect(addOutput)
        QgsApplication.taskManager().addTask(inference_task)

    def updateLayer(self):
        """Update the current layer"""
        layer = self.mapCanvas.currentLayer()
        if layer:
            if isinstance(layer.dataProvider(), QgsRasterDataProvider):
                self.layer = layer
            else:
                self.layer = None
        else:
            self.layer = None

    def run(self):
        """Main process.

        :raises Exception: if there is no item in the feed
        """
        try:
            self.log(
                message=self.tr(
                    text="Everything ran OK.",
                    context="QdeeplandiaPlugin",
                ),
                log_level=3,
                push=False,
            )
        except Exception as err:
            self.log(
                message=self.tr(
                    text="Houston, we've got a problem: {}".format(err),
                    context="QdeeplandiaPlugin",
                ),
                log_level=2,
                push=True,
            )
