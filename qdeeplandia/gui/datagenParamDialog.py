"""Design the data generation parameter setting dialog
"""

from qgis.PyQt.QtWidgets import (
    QDialog,
    QDialogButtonBox,
    QHBoxLayout,
    QLabel,
    QSpinBox,
    QVBoxLayout,
)


class DatagenParamDialog(QDialog):
    def __init__(self, parent):
        super(DatagenParamDialog, self).__init__()

        self.vlayout = QVBoxLayout(self)
        self.hlayout_dpath = QHBoxLayout()
        self.vlayout.addLayout(self.hlayout_dpath)
        self.hlayout_imagesize = QHBoxLayout()
        self.vlayout.addLayout(self.hlayout_imagesize)

        self.imagesize_label = QLabel(self.tr("Image size (in pixels):"))
        self.hlayout_imagesize.addWidget(self.imagesize_label)
        self.image_size = QSpinBox()
        self.image_size.setMinimum(16)
        self.image_size.setMaximum(1024)
        self.image_size.setValue(512)
        self.hlayout_imagesize.addWidget(self.image_size)

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        self.vlayout.addWidget(self.buttonBox)

    def datapath(self):
        return self.dataset_path.filePath()

    def imsize(self):
        return int(self.image_size.value())

    def batchsize(self):
        return int(self.batch_size.value())
