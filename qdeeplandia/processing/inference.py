"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

import os
from pathlib import Path

from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingContext,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterNumber,
    QgsProcessingParameterRasterLayer,
    QgsRasterLayer,
    QgsVectorLayer,
)
from qgis.PyQt.QtCore import QCoreApplication


class InferenceQDeepLandiaProcessingAlgorithm(QgsProcessingAlgorithm):
    """Infer building footprints on a raster layer built from an aerial image"""

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = "INPUT"
    IMAGE_SIZE = "IMAGE_SIZE"
    BATCH_SIZE = "BATCH_SIZE"
    VECTOR_OUTPUT = "VECTOR_OUTPUT"
    RASTER_OUTPUT = "RASTER_OUTPUT"
    OPEN_VECTOR_LAYER = "OPEN_VECTOR_LAYER"
    OPEN_RASTER_LAYER = "OPEN_RASTER_LAYER"

    def __init__(self, model=None):
        super().__init__()

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return InferenceQDeepLandiaProcessingAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "InferenceQDeepLandia"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Inference")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("QDeepLandia")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "QDeepLandia"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("Do inference according to the loaded model")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """
        self.addParameter(
            QgsProcessingParameterRasterLayer(self.INPUT, self.tr("Input layer"))
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.IMAGE_SIZE, self.tr("Image size (in pixels)"), defaultValue=512
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.BATCH_SIZE,
                self.tr("Batch size (number of images treated simultaneously)"),
                defaultValue=2,
            )
        )
        self.addParameter(
            QgsProcessingParameterBoolean(
                self.OPEN_VECTOR_LAYER,
                self.tr("Open vector layer (.geojson)?"),
                defaultValue=True,
            )
        )
        self.addParameter(
            QgsProcessingParameterBoolean(
                self.OPEN_RASTER_LAYER,
                self.tr("Open raster layer (.tif)?"),
                defaultValue=False,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        feedback.pushInfo("Begin the inference processing...")
        raster_in = self.parameterAsRasterLayer(parameters, self.INPUT, context)
        imsize = self.parameterAsString(parameters, self.IMAGE_SIZE, context)
        batch_size = self.parameterAsString(parameters, self.BATCH_SIZE, context)
        open_vector_layer = self.parameterAsBoolean(
            parameters, self.OPEN_VECTOR_LAYER, context
        )
        open_raster_layer = self.parameterAsBoolean(
            parameters, self.OPEN_RASTER_LAYER, context
        )

        # Path management
        raster_in_path = Path(raster_in.source())
        basename = raster_in_path.stem
        data_rootdir = raster_in_path.parents[3]
        datapath = data_rootdir.parent.name
        dataset = data_rootdir.name
        prediction_name = basename + "_" + str(imsize)
        output_basename = prediction_name + ".{extension}"
        output_filepath = str(
            data_rootdir
            / "output"
            / "semseg"
            / "predicted_{output_type}"
            / output_basename
        )
        vector_output_path = output_filepath.format(
            output_type="geometries", extension="geojson"
        )
        raster_output_path = output_filepath.format(
            output_type="rasters", extension="tif"
        )

        # Run deeposlandia as a separate process
        # "deepo geoinfer" is available for deeposlandia>0.7.1
        os.system(
            f"deepo geoinfer -P {datapath} -D {dataset} -b {batch_size} -i {basename} -s {imsize}"
        )

        # Prepare the output layers for introducing them onto the QGIS project
        if open_vector_layer:
            feedback.pushInfo("Add the .geojson output as a vector layer!")
            vector_layer = QgsVectorLayer(
                vector_output_path, prediction_name + "_vect", "ogr"
            )
            context.addLayerToLoadOnCompletion(
                vector_output_path,
                QgsProcessingContext.LayerDetails(
                    vector_layer.name(),
                    context.project(),
                    self.VECTOR_OUTPUT,
                ),
            )
        if open_raster_layer:
            feedback.pushInfo("Add the .tif output as a raster layer!")
            raster_layer = QgsRasterLayer(raster_output_path, prediction_name + "_rast")
            context.addLayerToLoadOnCompletion(
                raster_output_path,
                QgsProcessingContext.LayerDetails(
                    raster_layer.name(),
                    context.project(),
                    self.RASTER_OUTPUT,
                ),
            )
        return {
            self.VECTOR_OUTPUT: vector_output_path,
            self.RASTER_OUTPUT: raster_output_path,
        }
